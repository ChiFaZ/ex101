var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/',async function(req, res, next) {
  const { insertOrdersData } = require('../controllers/ordersData')
  await insertOrdersData()
  res.render('index', { title: 'Example', msgOrder: 'import orders data success!' });
});

module.exports = router;