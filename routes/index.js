var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', async function(req, res, next) {
  // const { insertRegionsData } = require('../controllers/regionsData')
  // await insertRegionsData()
  // const { insertOrdersData } = require('../controllers/ordersData')
  // await insertOrdersData()
  res.render('index', { title: 'Example' });
});

module.exports = router;
