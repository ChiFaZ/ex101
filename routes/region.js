var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', async function(req, res, next) {
    const { insertRegionsData } = require('../controllers/regionsData')
    await insertRegionsData()
    res.render('index', { title: 'Example', msgOrder: 'import region data success!' });
});

module.exports = router;