const { connectDB } = require('./connectDB');
const fs = require('fs');
const tj = require('@mapbox/togeojson');
DOMParser = require('xmldom').DOMParser;


const insertRegionsData = async (data, result) => {

    const { Sequelize, Model, DataTypes } = require('sequelize');
    const sequelize = new Sequelize('postgresql://localhost:5432/appleclub')

    class User extends Model {}
    User.init({
    area_id: DataTypes.STRING,
    province_id: DataTypes.STRING,
    district_id: DataTypes.STRING,
    sub_district_id: DataTypes.STRING,
    province_name: DataTypes.STRING,
    province_name_th: DataTypes.STRING,
    district_name: DataTypes.STRING,
    district_name_th: DataTypes.STRING,
    sub_district_name: DataTypes.STRING,
    sub_district_name_th: DataTypes.STRING,
    postcode: DataTypes.STRING,
    }, { sequelize, modelName: 'regions_data' });

    (async () => {
        var kml = new DOMParser().parseFromString(fs.readFileSync('./data/region_data.kml', 'utf8'));

        var converted = tj.kml(kml);
        var convertedWithStyles = tj.kml(kml, { styles: true });

        await User.destroy({ truncate : true, cascade: false });
        await sequelize.query("ALTER SEQUENCE regions_data_id_seq RESTART WITH 1", {});

        await Promise.all(
          (convertedWithStyles.features).map(async (d) => {
            await insertData(d.properties, sequelize, User)
          })
      )

    })();
}

async function insertData (d, sequelize, User) {
    await sequelize.sync();
    await User.create({
        area_id: d.area_id,
        province_id: d.province_id,
        district_id: d.district_id,
        sub_district_id: d.sub_district_id ,
        province_name: d.province_name,
        province_name_th: d.province_name_th,
        district_name: d.district_name,
        district_name_th: d.district_name_th,
        sub_district_name: d.sub_district_name,
        sub_district_name_th: d.sub_district_name_th,
        postcode: d.postcode
    });
}

module.exports = {
  insertRegionsData
}