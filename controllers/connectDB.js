
const connectDB = async (data, result) => {
    const { Sequelize } = require('sequelize');

    const sequelize = new Sequelize('postgresql://localhost:5432/appleclub')

    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

module.exports = {
    connectDB
}