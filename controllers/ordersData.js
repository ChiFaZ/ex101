const { connectDB } = require('../controllers/connectDB');
const fs = require('fs');


const insertOrdersData = async (data, result) => {

    const { Sequelize, Model, DataTypes } = require('sequelize');
    const sequelize = new Sequelize('postgresql://localhost:5432/appleclub')

    class User extends Model {}
    User.init({
    lat: DataTypes.STRING,
    lng: DataTypes.STRING,
    total: DataTypes.STRING,
    }, { sequelize, modelName: 'orders_data' });

    (async () => {

        const jsonData = fs.readFileSync('./data/orders_data.csv', 'utf8');
        const json = csvJSON(jsonData);
        
        await User.destroy({ truncate : true, cascade: false });
        await sequelize.query("ALTER SEQUENCE orders_data_id_seq RESTART WITH 1", {});

        await Promise.all(
            json.map(async (d) => {
              await insertData(d, sequelize, User)
            })
        )

    })();
}

function csvJSON (csv) {
    const lines = csv.split(/\r\n|\n/)
    const result = []
    const headers = lines[0].split(',')

    for (let i = 1; i < lines.length; i++) {        
        if (!lines[i])
            continue
        const obj = {}
        const currentline = lines[i].split(',')

        for (let j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j]
        }
        result.push(obj)
    }
    return result
}

async function insertData (d, sequelize, User) {
    await sequelize.sync();
    await User.create({
        lat: (d['"lat"']).replace(/"/g, ''),
        lng: (d['"lng"']).replace(/"/g, ''),
        total: d['"total"']
    });
}

module.exports = {
    insertOrdersData
}